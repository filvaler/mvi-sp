# Valeriia Fil
13.12.2021

# Tema: Predikce biologické aktivity

- využiti strojového učení jako pomocny nastroj v farmaceutickem průmyslu
- predikce biologické aktivity na základě jeji struktury
- predikujeme pEC50 pro regresní problém
- predikujeme 0/1(neaktivní/aktivní) pro klasifikační problém

# Řešení:

- Uprava dat: převedení smiles na Morganův fingerprint velikosti 1024 bitů
- Použité modely: NN, LSTM, GRU, XGBoost klasifikáční/regresní

# Data:

- data jsou přiložené z řešením, obsahuji sloupce smiles, chembl_id, potency, pec50, category
- důležite sloupečky jsou smiles a pec50, pro klasifikační úlohu pec50 převedeme na 0/1 podle nastaveného tresholdu, v notebooku je jako default nastaveno thre=6

# Pokyny ke spouštění:

- do prostředí přidejte vstupní data, pak je načtete v notebooku
- pak pustěté notebook a to všechno vypočítá, na koncí budete mít barplot porovnání jendotlivých použitych metod